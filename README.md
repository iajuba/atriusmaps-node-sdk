# atriusmaps-node-sdk

## To Install:

`npm install atriusmaps-node-sdk`

Or with yarn:

`yarn add atriusmaps-node-sdk`

Then within your code, import the map initializer via:

`import Init from 'atriusmaps-node-sdk'`

or use `require`

`const Init = require("atriusmaps-node-sdk")`

## To Use

The Init object contains 3 methods:

- `Init.setLogging(boolean logging)` : To turn on/off the logging
- `Init.getVersion()` : Returns the current version of the library
- `Init.newMap(Object configuration)` : This is how  your initialize a new map. This returns a Promise that resolves to your map.

The configuration object is documented in full at https://locusmapsjs.readme.io/docs/sdk-configuration - with the only difference being that headless is forced to true for this node (non-browser) version.

At a minimum, a configuration would contain an accountId and a venueId:

```js
const config = {
			venueId: '<venueId>',
			accountId: '<accountId>'
		}
```

You then initialize your map:

```js
const map = await Init.newMap(config)
```

Your map function is ready to receive commands – of which the following are currently supported:

- `help` : Returns a string indicating all available commands and their arguments
- `getDirections`: Get time, distance and navigation steps from one point to another
- `getPOIDetails`: Get detailed information about a POI by ID
- `getAllPOIs`: Get a list of all POIs for the venue
- `getStructures`: Returns a list of structures (buildings) within the venue along with their properties
- `getVenueData`: Returns a complete venue object containing all venue details
- `search`: Performs a search against a term specified

For details on these commands, including their arguments, return value formats, and examples, see https://locusmapsjs.readme.io/docs/commands

Note that all these commands are asynchronous, and return a promise. So use them with `await` or a `then` clause.

Examples:

```js
const poi = await map.getPOIDetails(11)
console.log(`Got POI details for ${poi.name}.`)
```

Or

```js
map.getPOIDetails(11)
  .then(poi => console.log(`Got POI Details for ${poi.name}.`))
```

For example:

```bash
node main.js
```
