import json from '@rollup/plugin-json'
// import { terser } from 'rollup-plugin-terser'

export default {
	input: [
		'./lib/src/configs/sdkHeadless.json',
		'./lib/src/utils/observable.js',
		'./lib/plugins/clientAPI/src/clientAPI.js',
		'./lib/plugins/dynamicPois/src/dynamicPois.js',
		'./lib/plugins/poiDataManager/src/poiDataManager.js',
		'./lib/plugins/sdkServer/src/sdkServer.js',
		'./lib/plugins/searchService/src/searchService.js',
		'./lib/plugins/venueDataLoader/src/venueDataLoader.js',
		'./lib/plugins/wayfinder/src/wayfinder.js',
		'./lib/deploy/nodeEntry.js'
	],
	output: {
		format: 'cjs',
		plugins: [
			// terser()
		],
		inlineDynamicImports: false,
		preserveModules: true,
		preserveModulesRoot: "lib",
		// file: 'dist/cjs/atriusmaps.js'
		dir: 'dist/cjs/'
	},
	plugins: [
		json()
	]
}
