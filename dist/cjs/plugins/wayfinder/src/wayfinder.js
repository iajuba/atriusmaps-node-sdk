'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('ramda');
var a = require('zousan');
var buildStructureLookup = require('../../../src/utils/buildStructureLookup.js');
var geodesy = require('../../../src/utils/geodesy.js');
var findRoute = require('./findRoute.js');
var navGraph = require('./navGraph.js');
var navGraphDebug = require('./navGraphDebug.js');
var segmentBuilder = require('./segmentBuilder.js');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);
var a__default = /*#__PURE__*/_interopDefaultLegacy(a);

const u={SECURITY:"SecurityLane",IMMIGRATION:"ImmigrationLane"};function d(d,p){const l=d.log.sublog("wayfinder"),c=async()=>{d.bus.send("venueData/loadNavGraph");};let f=new a__default["default"];d.bus.on("wayfinder/_getNavGraph",(()=>f)),d.bus.on("venueData/navGraphLoaded",(async({navGraphData:t,structures:n})=>{const e=buildStructureLookup.buildStructuresLookup(n),i=await y(),a=navGraph.createNavGraph(t,e.floorIdToOrdinal,e.floorIdToStructureId,i);f.resolve(a);}));const y=async()=>{const n=await d.bus.get("poi/getByCategoryId",{categoryId:"security"});return e__namespace.pipe(e__namespace.map(m),e__namespace.filter(e__namespace.identity))(n)},m=n=>n.queue&&{type:e__namespace.path(["queue","queueType"],n),id:e__namespace.path(["queue","queueSubtype"],n)};d.bus.on("wayfinder/showNavLineFromPhysicalLocation",(async({toEndpoint:t,selectedSecurityLanes:n=null,requiresAccessibility:o})=>async function(t,n,o){const e=await b({fromEndpoint:t,toEndpoint:n,options:o});if(e){const{segments:t}=e;o.primary&&d.bus.send("map/resetNavlineFeatures"),d.bus.send("map/showNavlineFeatures",{segments:t,alternative:!o.primary});}return e}(await d.bus.get("user/getPhysicalLocation"),t,{selectedSecurityLanes:n,requiresAccessibility:o,primary:!0})));const g=(t,n)=>d.bus.get("poi/getById",{id:t}).then((o=>{if(o&&o.position)return w(o,n);throw Error("Unknown POI ID "+t)}));const I=["lat","lng","floorId","ordinal"],h=e__namespace.pipe(e__namespace.pick(I),e__namespace.keys,e__namespace.propEq("length",I.length),Boolean),w=(t,n)=>({lat:t.position.latitude,lng:t.position.longitude,floorId:t.position.floorId,ordinal:n(t.position.floorId),title:t.name});async function b({fromEndpoint:t,toEndpoint:n,options:o={}}){return f.then((async e=>{o.compareFindPaths=p.compareFindPaths;const r=findRoute.findRoute(e,t,n,o);if(!r)return null;v(t,n,r);const a=await d.bus.get("venueData/getFloorIdToNameMap"),u=await d.bus.get("venueData/getQueueTypes"),c=d.gt(),{steps:f,segments:y}=segmentBuilder.buildSegments(r.waypoints,t,n,a,c,u);l.info("route",r);const m=Math.round(r.waypoints.reduce(((t,{eta:n})=>t+n),0)),g=Math.round(r.waypoints.reduce(((t,{distance:n})=>t+n),0));return {...r,segments:y,steps:f,time:m,distance:g}}))}d.bus.on("wayfinder/getNavigationEndpoint",(({ep:t})=>async function(t){return f.then((n=>{if(!t)throw Error("wayfinder: Invalid endpoint definition",t);if("number"==typeof t)return g(t,n.floorIdToOrdinal);if("string"==typeof t){if(t.match(/^\d+$/))return g(parseInt(t),n.floorIdToOrdinal);if(t.indexOf(",")>0){let[o,e,i,r]=t.split(",");if(!n.floorIdToStructureId(i))throw Error("Unknown floorId in endpoint: "+i);return r||(r="Starting Point"),{lat:parseFloat(o),lng:parseFloat(e),ordinal:n.floorIdToOrdinal(i),floorId:i,title:r}}}if(h(t))return t;if(t.latitude)return {lat:t.latitude,lng:t.longitude,floorId:t.floorId,ordinal:n.floorIdToOrdinal(t.floorId),title:t.title};if(t.position&&t.name)return w(t,n.floorIdToOrdinal);throw Error("Invalid start or end point: "+t)}))}(t))),d.bus.on("wayfinder/checkIfPathHasSecurity",(({fromEndpoint:n,toEndpoint:o,options:e={}})=>f.then((r=>{e.compareFindPaths=p.compareFindPaths;const a=findRoute.findRoute(r,n,o,e);if(!a)return {routeExists:!1};const s=n=>Boolean(a.waypoints.find(e__namespace.pathEq(["securityLane","type"],n)));return {routeExists:!0,queues:a.waypoints.filter((n=>e__namespace.pathEq(["securityLane","type"],u.SECURITY,n)||e__namespace.pathEq(["securityLane","type"],u.IMMIGRATION,n))),hasSecurity:s(u.SECURITY),hasImmigration:s(u.IMMIGRATION)}})))),d.bus.on("wayfinder/getRoute",b);const v=(t,n,o)=>d.bus.send("session/submitEvent",{type:"navigation",startPosition:{venueId:t.floorId.split("-")[0],buildingId:o.waypoints[0].position.structureId,floorId:o.waypoints[0].position.floorId,lat:o.waypoints[0].position.lat,lng:o.waypoints[0].position.lng},endPosition:{venueId:n.floorId.split("-")[0],buildingId:o.waypoints[o.waypoints.length-1].position.structureId,floorId:o.waypoints[o.waypoints.length-1].position.floorId,lat:o.waypoints[o.waypoints.length-1].position.lat,lng:o.waypoints[o.waypoints.length-1].position.lng}});function T(t){return S(t,"transitTime")}function E(t){return S(t,"distance")}function S(n,o){return e__namespace.aperture(2,n).map((([n,o])=>{return (e=o.id,n=>e__namespace.find((t=>t.dst===e),n.edges))(n);var e;})).map(e__namespace.prop(o)).reduce(((t,n)=>t+n),0)}function L(t,n){return t.distance=geodesy.distance(n.lat,n.lng,t.position.latitude,t.position.longitude),t}return d.bus.on("wayfinder/addPathTimeSingle",(async({poi:t,startLocation:n,options:o={}})=>n?f.then((e=>function(t,n,o,e){const i=w(o,t.floorIdToOrdinal),r=t.findShortestPath(e,i,n);if(!r)return (o=L(o,e)).transitTime=o.distance/60,o;return Object.assign(o,{transitTime:T(r),distance:E(r)})}(e,o,t,n))):t)),d.bus.on("wayfinder/addPathTimeMultiple",(async({pois:t,startLocation:n,options:o={}})=>n?f.then((e=>function(t,n,o,e){try{const i=o.map((n=>w(n,t.floorIdToOrdinal))),r=t.findAllShortestPaths(e,i,n);return o.map(((t,n)=>function(t,n,o){n&&n.length?t=Object.assign(t,{transitTime:T(n),distance:E(n)}):(t=L(t,o)).transitTime=t.distance/60;return t}(t,r[n],e)))}catch(t){return l.error(t),o}}(e,o,t,n))):t)),d.bus.on("venueData/loadNewVenue",(()=>{f=new a__default["default"],c();})),d.bus.on("poi/setDynamicData",(({plugin:t,idValuesMap:n})=>{"security"===t&&f.then((t=>t.updateWithSecurityWaitTime(n)));})),d.bus.on("wayfinder/getNavGraphFeatures",(()=>f.then((({_nodes:t})=>navGraphDebug.enrichDebugNavGraph(t))))),{init:c,internal:{resolveNavGraph:t=>f.resolve(t),prepareSecurityLanes:y}}}

exports.SecurityLaneType = u;
exports.create = d;
