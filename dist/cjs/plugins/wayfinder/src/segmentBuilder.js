'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('ramda');
var geom = require('../../../src/utils/geom.js');
var segmentCategories = require('./segmentCategories.js');
var stepBuilder = require('./stepBuilder.js');

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);

const n=e__namespace.map(e__namespace.converge(e__namespace.assoc("coordinates"),[e=>e.waypoints.flatMap(((e,o)=>o>0&&e.curvedPathForward&&e.curvedPathForward.length>0?e.curvedPathForward.flatMap((e=>geom.bezierCurveTo(e.start.lng,e.start.lat,e.in.lng,e.in.lat,e.out.lng,e.out.lat,e.end.lng,e.end.lat))).map((e=>[e.x,e.y])):[[e.position.lng,e.position.lat]])),e__namespace.identity])),r=t=>{const a=[];let n={segmentCategory:void 0,waypoints:[]},r=null,s=[];return n.waypoints=[t[0]],n.type=t[0].isPortal?t[0].portalType:"Walk",a.push(n),n={segmentCategory:void 0,waypoints:[]},t.forEach((e=>{s.push(e),r?(r.isPortal===e.isPortal&&r.isSecurityCheckpoint===e.isSecurityCheckpoint||(n.waypoints=s,e.isPortal||r.isPortal?(s.length>1&&s.pop(),s=!e.isPortal||"train"!==e.portalType.toLowerCase()&&"bus"!==e.portalType.toLowerCase()?[e]:[s[s.length-1],e]):s=[],r.poiId&&(n.poiId=r.poiId),a.push(n),n={segmentCategory:void 0,waypoints:[]},n.type=e.isPortal?e.portalType:"Walk"),n.levelDifference=e.levelDifference,r=e):(n.type=e.isPortal?e.portalType:"Walk",r=e);})),n.waypoints=s,0===s.length&&(n.waypoints=[r]),a.push(n),(e=>{e.forEach(((t,a)=>{0===a?t.segmentCategory=segmentCategories.START:t.waypoints[t.waypoints.length-1].isDestination?t.segmentCategory=segmentCategories.WALKING_TO_END:"Security Checkpoint"===t.type?t.segmentCategory=segmentCategories.SECURITY_CHECKPOINT:"Bus"===t.type?t.segmentCategory=segmentCategories.BUS:"Train"===t.type?t.segmentCategory=segmentCategories.TRAIN:"Stairs"===t.type?t.levelDifference>0?t.segmentCategory=segmentCategories.STAIRS_UP:t.levelDifference<0?t.segmentCategory=segmentCategories.STAIRS_DOWN:t.segmentCategory=segmentCategories.STAIRS:"Elevator"===t.type?t.levelDifference>0?t.segmentCategory=segmentCategories.ELEVATOR_UP:t.levelDifference<0?t.segmentCategory=segmentCategories.ELEVATOR_DOWN:t.segmentCategory=segmentCategories.ELEVATOR:"Escalator"===t.type?t.levelDifference>0?t.segmentCategory=segmentCategories.ESCALATOR_UP:t.levelDifference<0?t.segmentCategory=segmentCategories.ESCALATOR_DOWN:t.segmentCategory=segmentCategories.ESCALATOR:"Ramp"===t.type?t.levelDifference>0?t.segmentCategory=segmentCategories.RAMP_UP:t.levelDifference<0?t.segmentCategory=segmentCategories.RAMP_DOWN:t.segmentCategory=segmentCategories.RAMP:"Security Checkpoint"===e[a+1].type?t.segmentCategory=segmentCategories.WALKING_TO_SECURITY_CHECKPOINT:"Walk"!==e[a+1].type&&(t.segmentCategory=segmentCategories.WALKING_TO_PORTAL);}));})(a),((e,t)=>{if(1===e.length){const a={segmentCategory:void 0,waypoints:[]};a.segmentCategory=segmentCategories.WALKING_TO_END,a.type="Walk",a.waypoints=[t],e.push(a);}})(a,r),(t=>{t.forEach(((o,a)=>{if(a>1&&0===e__namespace.head(o.waypoints).levelDifference){const n=e__namespace.last(t[a-1].waypoints);o.waypoints=e__namespace.prepend(n,o.waypoints);}}));})(a),a},s=(t,s,i,p,l,g)=>{let y=r(t);y=n(y),s&&y[0].coordinates.unshift([s.lng,s.lat]),i&&e__namespace.last(y).coordinates.push([i.lng,i.lat]);return {segments:y.map(((t,a)=>{const n=e__namespace.last(t.waypoints),r=t.coordinates,s=!(t.levelDifference&&t.waypoints.every(e__namespace.prop("isPortal"))),i={levelId:n.position.structureId,ordinalId:`ordinal: ${n.position.ordinal}`,coordinates:r,shouldDrawSegment:s},p=[];if(segmentCategories.WALKING_TO_PORTAL===t.segmentCategory){const t=y[a+1];p.push({canonicalName:`wayfinding.${t.segmentCategory}`,coordinates:e__namespace.last(r)});}else segmentCategories.START!==t.segmentCategory&&p.push({canonicalName:`wayfinding.${t.segmentCategory}`,coordinates:e__namespace.last(r)});return i.badges=p,i.segmentType=(e=>"Train"===e.type?"nav.train":"Bus"===e.type?"nav.transit":"Security Checkpoint"===e.type?"nav.secure":"nav.primary")(t),t.poiId&&(i.poiId=t.poiId),i})),steps:stepBuilder(y,e__namespace.prop("title",s),e__namespace.prop("title",i),p,l,g)}};

exports.buildSegments = s;
