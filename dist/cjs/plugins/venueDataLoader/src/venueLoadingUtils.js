'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('ramda');
var bounds = require('../../../src/utils/bounds.js');
var configUtils = require('../../../src/utils/configUtils.js');

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);

const s=async(t,a)=>t?fetch(a,{headers:{Authorization:t}}):fetch(a),o=t=>a=>s(t,a).then((t=>t.json())),c=t=>a=>s(t,a).then((t=>t.text())),n=t=>`https://api.content.locuslabs.com/${t}`,r=(a,e,s,o,c)=>e__namespace.mapObjIndexed(((t,e)=>((t,a,e,s,o,c)=>"theme"===c||"style"===c?`${n(e)}/${a}/${c}/${o}/${s}/${c}.json`:t.replace(/https:\/\/content.locuslabs.com/gi,n(e)))(t,a,s,o,c,e)),e),u=async(t,a)=>{const s={alpha:"alpha-a.locuslabs.com",beta:"beta-a.locuslabs.com",gamma:"gamma-a.locuslabs.com",prod:"a.locuslabs.com"},{assetStage:o,venueId:c,accountId:n,formatVersion:u}=t,l=`https://${s[o]||s.prod}/accounts/${n}`,i=u||"v5",h=t.dataFetch&&configUtils.global[t.dataFetch]&&configUtils.global[t.dataFetch].getFiles?await configUtils.global[t.dataFetch].getFiles(t):await a(`${l}/${i}.json`);if(!h[c])throw Error(`Attempt to access venue ${c} which is not within venue list: ${Object.keys(h)}`);const d=h[c].files,m=(t.dataFetch&&configUtils.global[t.dataFetch]&&configUtils.global[t.dataFetch].getVenueData?await configUtils.global[t.dataFetch].getVenueData(t):await a(d.venueData))[c];m.venueList=h;const p=(t=>{const a=t.deepLinkProps?t.deepLinkProps.contentStage:null;return "alpha"===a||"beta"===a||"prod"===a?a:null})(t);return m.files=p?r(m.category,d,p,n,c):d,m},l=t=>{const{structureOrder:e,structures:s}=t;return e.map((t=>{const e=s[t],o=bounds.findBoundsOfCoordinates(e.boundsPolygon);return {...e,bounds:o}}))};

exports.buildStructures = l;
exports.createFetchJson = o;
exports.createFetchText = c;
exports.getVenueDataFromUrls = u;
