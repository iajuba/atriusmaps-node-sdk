'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('ramda');
var util = require('./util.js');

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);

const s=[{command:"destroy"},{command:"getDirections",args:[{name:"from",type:"location"},{name:"to",type:"location"},{name:"accessible",type:"boolean",optional:!0},{name:"queueTypes",type:"list",itemType:{type:"string"},optional:!0}]},{command:"getPOIDetails",args:[{name:"poiId",type:"integer",min:0}]},{command:"getAllPOIs"},{command:"getStructures"},{command:"getVenueData"},{command:"search",args:[{name:"term",type:"string",minLength:2},{name:"details",type:"boolean",optional:!0}]}];function a(s){s.bus.on("clientAPI/destroy",(async()=>s.destroy())),s.bus.on("clientAPI/getDirections",(async({from:n,to:a,accessible:o,queueTypes:i})=>{const r=await util.locationToEndpoint(s,n),c=await util.locationToEndpoint(s,a),u={requiresAccessibility:!!o};return i&&(u.selectedSecurityLanes={SecurityLane:i}),s.bus.get("wayfinder/getRoute",{fromEndpoint:r,toEndpoint:c,options:u}).then(e__namespace.pick(["distance","time","steps","navline","waypoints"]))})),s.bus.on("clientAPI/getPOIDetails",(async({poiId:e})=>s.bus.get("poi/getById",{id:e}))),s.bus.on("clientAPI/getAllPOIs",(async()=>s.bus.get("poi/getAll"))),s.bus.on("clientAPI/getStructures",(()=>util.getStructures(s)));const a=e=>"function"!=typeof e;s.bus.on("clientAPI/getVenueData",(async()=>{const t=await s.bus.get("venueData/getVenueData");return e__namespace.filter(a,t)})),s.bus.on("clientAPI/search",(async({term:e,details:t})=>s.bus.get("search/queryAsync",{term:e}).then((n=>{const a=n.map((e=>e.poiId));return s.bus.send("event/search",{referrer:"prog",searchMethod:null,query:e,entities:a}),t?n:a}))));}

exports.handleHeadless = a;
exports.headlessCommands = s;
