'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('flexsearch');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var e__default = /*#__PURE__*/_interopDefaultLegacy(e);

const a=["ko","ja","zh-Hans","zh-Hant"],n={encode:!1,split:/\s+/,tokenize:"full"},t={standard:{profile:"match"},typeahead:{encode:"advanced"},full:n},d=({lang:d,type:o="standard"})=>{const l=a.includes(d)?n:t[o];return new e__default["default"]({...l})};

exports.getFlexSearchInstance = d;
