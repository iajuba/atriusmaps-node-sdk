'use strict';

var e = require('ramda');
var utils = require('./utils.js');

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);

function t(t,e){const o=utils.getFlexSearchInstance({lang:e});function n(r){return Object.values(r).map((r=>{const{poiId:t,category:e="",name:o,keywords:n=[],roomId:c=""}=r,i=e__namespace.path(["dynamicData","grab","tags"],r)||[],p=n.filter(e__namespace.prop("isUserSearchable")).map(e__namespace.prop("name")),s=`${o} ${e.split(".").join(" ")} ${c} ${p.join(" ")} ${i.join(" ")}`;return [Number(t),s]}))}return o.addMatcher({"['.,]":""}),n(t).forEach((([a,r])=>o.add(a,r))),{search:function(r){const e=o.search(r);return Object.values(e__namespace.pick(e,t))},updateMultiple:function(a){n(a).forEach((([a,r])=>o.update(a,r)));}}}

module.exports = t;
