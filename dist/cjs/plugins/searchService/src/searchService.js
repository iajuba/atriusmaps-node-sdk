'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('ramda');
var a = require('zousan');
var configUtils = require('../../../src/utils/configUtils.js');
var rand = require('../../../src/utils/rand.js');
var poiSearch = require('./poiSearch.js');
var searchTypeahead = require('./searchTypeahead.js');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);
var a__default = /*#__PURE__*/_interopDefaultLegacy(a);

function o(o,i){const c={poiSearch:null,typeahead:null,indexesCreated:new a__default["default"],defaultSearchTerms:null,specialQueryTerms:{}},u=async()=>{const e=await o.bus.get("poi/getAll");c.poiSearch=poiSearch(e,o.i18n().language),c.typeahead=searchTypeahead(e,c.poiSearch.search,o.i18n().language),c.defaultSearchTerms=configUtils.getLocalized(i,"defaultSearchTerms",o.i18n().language),c.indexesCreated.resolve();};async function d(){const r=await o.bus.get("user/getPhysicalLocation"),a=await o.bus.get("poi/getByFloorId",{floorId:r.floorId}),s=Object.values(e__namespace.pickBy((e=>-1===e.category.indexOf("portal")&&"element.door"!==e.category),a)),t=await o.bus.get("wayfinder/addPathTimeMultiple",{pois:s,startLocation:r});return e__namespace.sortBy(e__namespace.prop("distance"),Object.values(t)).slice(0,50)}o.bus.on("search/queryNearby",(async()=>{const e=await d();return o.bus.send("search/showNearby",{pois:e,term:"Nearby"}),e})),o.bus.on("search/queryNearbyAsync",d),o.bus.on("search/queryCategory",(async({category:e,categoryName:r,searchTerm:a})=>{const s=await c.indexesCreated.then((()=>c.poiSearch.search({query:a||e})));return o.bus.send("search/showCategory",{pois:s,category:e,categoryName:r}),s})),o.bus.on("search/query",(({term:e})=>c.indexesCreated.then((()=>{const r=c.poiSearch.search({query:e});return o.bus.send("search/showSearchResults",{results:r,term:e}),r})))),o.bus.on("search/queryAsync",(({term:e})=>c.indexesCreated.then((()=>c.poiSearch.search({query:e}))))),o.bus.on("search/queryWithSpecial",(({term:e})=>{if(c.specialQueryTerms[e]){const{event:r,params:a}=c.specialQueryTerms[e];return o.bus.send(r,a)}return o.bus.get("search/query",{term:e})})),o.bus.on("search/getDefaultSearchTerms",(async({limit:e=5}={})=>{const r=c.defaultSearchTerms,a=r&&r.length?r:await async function(e){const r=(await o.bus.send("poi/getAllCategories"))[0],a=Array.from(new Set(r));return function(e){for(let r=e.length-1;r>0;r--){const a=Math.floor(Math.random()*(r+1));[e[r],e[a]]=[e[a],e[r]];}return e}(a).slice(0,e)}(e);return o.bus.send("search/showDefaultSearchKeywords",{keywords:a}),a})),o.bus.on("search/getDefaultSearchPois",(async({limit:r=5}={})=>{const a=await o.bus.get("poi/getAll"),t=e__namespace.pickBy(((e,r)=>e.isNavigable),a);return rand.arrayPick(Object.values(t),r)})),o.bus.on("search/registerSpecialQuery",(({term:e,event:r,params:a,addKeyword:s=!0})=>{c.indexesCreated.then((()=>{s&&c.typeahead.addKeyword(e),c.specialQueryTerms[e]={event:r,params:a};}));})),o.bus.on("search/addKeywords",(({keywords:e})=>c.indexesCreated.then((()=>e.forEach((e=>c.typeahead.addKeyword(e))))))),o.bus.on("search/typeahead",(({term:e,limit:r})=>c.indexesCreated.then((()=>{const{keywords:a,pois:s}=c.typeahead.query(e,r);return {keywords:a,pois:s,term:e}})))),o.bus.on("venueData/loadNewVenue",(()=>{c.indexesCreated=new a__default["default"],u();})),o.bus.on("poi/setDynamicData",(async({plugin:e,idValuesMap:r})=>{if("grab"!==e)return;const a=Object.keys(r).map((e=>o.bus.get("poi/getById",{id:e})));return Promise.all(a).then((e=>c.indexesCreated.then((()=>c.poiSearch.updateMultiple(e)))))}));return {init:u,runTest:async(e,r)=>(await r(),c)}}

exports.create = o;
