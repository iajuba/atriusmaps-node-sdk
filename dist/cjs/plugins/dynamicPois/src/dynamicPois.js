'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('ramda');
var a$1 = require('zousan');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);
var a__default = /*#__PURE__*/_interopDefaultLegacy(a$1);

function a(a,s){let n=new a__default["default"];const i=new a__default["default"];function o(...e){r(...e),p(...e);}function r(t,s){const n=e__namespace.pipe(e__namespace.filter((e=>"parking"===e.category)),e__namespace.map((a=>{const s=a.dynamicAttributes;if(!s)throw Error(`No dynamicAttributes defined for parking POI ${a.poiId}`);return {...(t-a.timestamp)/1e3<s["parking.timeToLive"]?e__namespace.pick(["lotStatus","rateDay","rateHour","timeIsReal","timeToTerminal1","timeToTerminal2"],a):{lotStatus:s["parking.default"],rateDay:"$ -",rateHour:"$ -",timeIsReal:!1},lastUpdated:a.timestamp,lotName:a.lotName}})))(s);a.bus.send("poi/setDynamicData",{plugin:"parking",idValuesMap:n});}function u(t,s){const n=e__namespace.pipe(e__namespace.map((e=>[e.poiId,l(t,e)])),e__namespace.fromPairs)(s);a.bus.send("poi/setDynamicData",{plugin:"security",idValuesMap:n});}const l=(e,t)=>({queueTime:t.queueTime,isTemporarilyClosed:t.isTemporarilyClosed,timeIsReal:!t.isQueueTimeDefault&&t.expiration>e,lastUpdated:e});function p(t,s){const n=["dynamicData","openClosed"],i=e__namespace.filter(e__namespace.hasPath(n),s),o=e__namespace.map(e__namespace.path(n),i);if(!e__namespace.all(e__namespace.both(e__namespace.has("isOpen"),e__namespace.has("expiration")),e__namespace.values(o)))throw Error("Open Closed poi status is malformed.");{const s=e__namespace.pipe(e__namespace.prop("expiration"),e__namespace.lt(t)),n=e__namespace.filter(s,o);a.bus.send("poi/setDynamicData",{plugin:"open-closed-status",idValuesMap:n});}}return a.bus.on("venueData/venueDataLoaded",(({venueData:e})=>{n.v?n=a__default["default"].resolve(e):n.resolve(e);})),a.bus.on("sdk/readyWhenYouAre",(()=>i)),{init:async()=>{const e=s.urlBase||"https://rest.locuslabs.com/v3",t=s.urlBaseV1||s.urlBase||"https://rest.locuslabs.com/v1",r=a.config.plugins.venueDataLoader.accountId;async function l(){return n.then((e=>{let a=`${t}/venue/${e.id}/account/${r}/get-all-dynamic-pois/`;return (t.startsWith("./")||t.endsWith(".json"))&&(a=t),a}))}const p=async()=>n.then((t=>e.startsWith("./")||e.endsWith(".json")?e:`${e}/venueId/${t.id}/accountId/${r}/get-dynamic-queue-times/`)),c=async()=>{Promise.all([l().then(fetch).then((e=>e.json())).then((e=>o(Date.now(),e))),p().then(fetch).then((e=>e.json())).then((e=>u(Date.now(),e)))]).then((()=>i.resolve(!0))).catch((e=>{console.error(e),i.resolve(!0);}));},m=await a.bus.get("venueData/getQueueTypes");m.SecurityLane&&m.SecurityLane.length?n.then(c).then((()=>setInterval(c,3e4))):i.resolve(!0);},internal:{processSecurityWaitTimes:u,processParkingPOIS:r,processOpenClosedPois:p,processDynamicPois:o}}}

exports.create = a;
