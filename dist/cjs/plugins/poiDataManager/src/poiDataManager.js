'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('ramda');
var a = require('zousan');
var buildStructureLookup = require('../../../src/utils/buildStructureLookup.js');
var configUtils = require('../../../src/utils/configUtils.js');
var i18n = require('../../../src/utils/i18n.js');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);
var a__default = /*#__PURE__*/_interopDefaultLegacy(a);

async function n(a,n){const s=a.log.sublog("poiDataManager"),u=()=>{a.bus.send("venueData/loadPoiData");};let p=new a__default["default"];const d=(o,e)=>{const{position:t}=o,i=e.floorIdToStructure(t.floorId);if(!i)return s.error(`No structure found for floorId: ${t.floorId} for POI ${o.poiId}`),{...o};const a=e.floorIdToFloor(t.floorId),n={...t,structureName:i.name,buildingId:i.id,floorName:a.name,floorOrdinal:a.ordinal};return {...o,position:n}},c=(o,e)=>{o.roomInfo||(o.roomInfo=[]),o.roomInfo.push(e);},l=e__namespace.pipe(e__namespace.propOr([],"externalIds"),e__namespace.find(e__namespace.propEq("type","roomId")),e__namespace.prop("id"),e__namespace.unless(e__namespace.isNil,e__namespace.tail));a.bus.on("venueData/poiDataLoaded",(async({pois:e,structures:n})=>{if(e=((e,t)=>e__namespace.pipe(e__namespace.values,e__namespace.map((o=>{o.distance=null,o.isNavigable=void 0===o.isNavigable||!0===o.isNavigable,o.capacity&&c(o,{name:`Seats ${o.capacity.join("-")}`,svgId:"number-of-seats"}),o.category.startsWith("meeting")&&c(o,{name:a.gt()("poiView:Conference Room"),svgId:"conference-room"});const e=l(o);return e&&(o.roomId=e),[o.poiId,d(o,t)]})),e__namespace.fromPairs)(e))(e,buildStructureLookup.buildStructuresLookup(n)),configUtils.debugIsTrue(a,"pseudoTransPois"))for(const o in e)e[o]=r(e[o],a.i18n().language);e=function(o){const e=[];return Object.values(o).forEach((o=>{try{const t=o.position;t?["buildingId","structureName","floorId","floorName","floorOrdinal","latitude","longitude"].forEach((i=>{null!==t[i]&&void 0!==t[i]||e.push({id:o.poiId,e:`invalid position property: ${i}: ${t[i]}`});})):e.push({poi:o,e:"No position information"});}catch(t){s.error(t),e.push({id:o.poiId,e:t.message});}})),e.length&&(s.warn("badPois:",e),e.forEach((e=>{delete o[e.id];}))),o}(e),await async function(o){for(const e of Object.values(o))await y(e);return o}(e),p.resolve(e),a.config.debug&&a.env.isBrowser&&(window._pois=e),a.config.debug&&async function(o){const e=Date.now(),t=[],i=await a.bus.get("wayfinder/_getNavGraph");Object.values(o).forEach((o=>{try{const e=o.position;i.findClosestNode(e.floorId,e.latitude,e.longitude)||t.push({id:o.poiId,e:"No closest Navgraph Node"});}catch(e){s.error(e),t.push({id:o.poiId,e:e.message});}})),t.length&&s.warn("badPois:",t),s(`Total time for navgraph POI check: ${Date.now()-e}ms`);}(e);})),a.bus.on("poi/getById",(async({id:o})=>p.then((e=>e[o])))),a.bus.on("poi/getByFloorId",(async({floorId:e})=>p.then(e__namespace.pickBy(e__namespace.pathEq(["position","floorId"],e))))),a.bus.on("poi/getByCategoryId",(async({categoryId:e})=>p.then(e__namespace.pickBy((o=>o.category===e||o.category.startsWith(e+".")))))),a.bus.on("poi/getAll",(async()=>p));const f=["queue","primaryQueueId"],m=(e,t,i)=>{const a=e__namespace.path(["queue","queueType"],t);if(!a)return null;const n=e[a],r=e__namespace.path(f,t);return i.filter(e__namespace.pathEq(f,r)).filter((o=>o.poiId!==t.poiId)).map((e=>{const t=e__namespace.path(["queue","queueSubtype"],e),i=g(t)(n);return {poiId:e.poiId,...i}}))},g=e=>{return e__namespace.pipe(e__namespace.find(e__namespace.propEq("id",e)),(t=`No queue found with ID: ${e}`,o=>{if(null!=o)return o;throw Error(t)}),e__namespace.pick(["displayText","imageId"]));var t;};async function y(t){if(!t)return;const i="undefined"==typeof window?1:window.devicePixelRatio||1;return e__namespace.length(t.images)?t.images[0].startsWith("https:")||(t.images=await a__default["default"].all(t.images.map((o=>a.bus.get("venueData/getPoiImageUrl",{imageName:o,size:`${351*i}x${197*i}`}))))):t.images=[],t}a.bus.on("poi/addOtherSecurityLanes",(({poi:e})=>(async e=>{if(!e__namespace.path(f,e))return e;const t=await a.bus.get("venueData/getQueueTypes"),i=await a.bus.get("poi/getByCategoryId",{categoryId:"security"}),n=Object.values(i);return e.queue.otherQueues=m(t,e,n),e})(e)));const I=e__namespace.memoizeWith(e__namespace.identity,e__namespace.pipe(e__namespace.pluck("category"),e__namespace.values,e__namespace.uniq));a.bus.on("poi/getAllCategories",(async()=>p.then(I))),a.bus.on("venueData/loadNewVenue",(()=>{p=new a__default["default"],u();})),a.bus.on("poi/setDynamicData",(({plugin:e,idValuesMap:t})=>{p.then((i=>{for(const a in t){const n=i[a].dynamicData||{};n[e]={...t[a]};const r=e__namespace.mergeRight(i[a],{dynamicData:n});i[a]=r;}}));}));return {init:u,runTest:async o=>(await o(),p),internal:{addImages:y,pseudoTransPoi:r}}}function r(o,e){return ["description","nearbyLandmark","name","phone","operationHours"].forEach((t=>{o[t]&&(o[t]=i18n.toLang(o[t],e));})),o.keywords&&(o.keywords=o.keywords.map((o=>(o.name=i18n.toLang(o.name,e),o)))),o.position.floorName&&(o.position.floorName=i18n.toLang(o.position.floorName,e)),o.position.structureName&&(o.position.structureName=i18n.toLang(o.position.structureName,e)),o}

exports.create = n;
