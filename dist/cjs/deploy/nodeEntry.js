'use strict';

var e = require('node-fetch');
var package_json = require('../package.json.js');
var controller = require('../src/controller.js');
var observable = require('../src/utils/observable.js');
var prepareSDKConfig = require('./prepareSDKConfig.js');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var e__default = /*#__PURE__*/_interopDefaultLegacy(e);

const r=package_json["default"].version;globalThis.fetch||(globalThis.fetch=e__default["default"]);let c=!1;const i=e=>e.toString().length<2?"0"+e:e,a=()=>`AtriusMaps Node SDK (${(()=>{const e=new Date;return `${e.getHours()}:${i(e.getMinutes())}:${i(e.getSeconds())}.${e.getMilliseconds()}`})()}): `,g=function(){if(c){let e=a()+Array.from(arguments).map((e=>"object"==typeof e?JSON.stringify(e):e)).join(" ");const n=e.split("\n");n.length>1?e=n[0]+`… (+ ${n.length-1} lines)`:e.length>256&&(e=e.substring(0,255)+`… (length: ${e.length} chars)`),console.log(e);}};const l={getVersion:()=>r,newMap:e=>async function(e){return new Promise(((n,r)=>{e.headless=!0;const c=prepareSDKConfig(e);controller.create(c).then((e=>{const t=(n,t)=>("string"==typeof n&&(n={...t,command:n}),g("Sending command object: ",n),e.bus.get("clientAPI/execute",n).then((e=>(g("Received Message: ",e),e))).catch((e=>{throw g("Error: ",e.message),e})));observable(t),e.eventListener.observe((function(){t.fire.apply(t,arguments);})),t.on("ready",((e,o)=>{const{commands:s}=o.commandJSON;!function(e,n){n.forEach((n=>{e[n.command]=function(){const t={command:n.command};for(let e=0;e<arguments.length;e++)t[n.args[e].name]=arguments[e];return e(t)};}));}(t,s),n(t),g("map ready");}));}));}))}(e),setLogging:e=>{c=e,e&&g(`Atrius Maps JS SDK Client v${r} Logging enabled.`);}};

module.exports = l;
