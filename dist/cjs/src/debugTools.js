'use strict';

var e = require('ramda');
var navGraphDebug = require('../plugins/wayfinder/src/navGraphDebug.js');
var dom = require('./utils/dom.js');
var funcs = require('./utils/funcs.js');

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);

"undefined"!=typeof window&&(window.R=e__namespace);const r=(e,n)=>{const s=n.monitor("homeview/performSearch",(({term:t})=>{t||(n.send(e),s());}));};var i={showIcons:()=>{dom.$("#mapRenderDiv").innerHTML="<style> div { display: inline-block; text-align: center; border: 1px solid lightblue; }</style>"+dom.$$("svg symbol").map((e=>`<div><svg><use xlink:href="#${e.id}"/></svg><br/>${e.id}</div>`)).join("");},poisByCategory:function(){return this.bus.send("poi/getAll").then((e=>e[0])).then((e=>Object.values(e))).then(e__namespace.groupBy((e=>e.category)))},highlightNodes:()=>dom.ad({tag:"style",html:"* { background-color: rgba(255,0,0,.2); } * * { background-color: rgba(0,255,0,.2); } * * * { background-color: rgba(0,0,255,.2); } * * * * { background-color: rgba(255,0,255,.2); } * * * * * { background-color: rgba(0,255,255,.2); } * * * * * * { background-color: rgba(255,255,0,.2); } * * * * * * * { background-color: rgba(255,0,0,.2); } * * * * * * * * { background-color: rgba(0,255,0,.2); } * * * * * * * * * { background-color: rgba(0,0,255,.2); }"},dom.$("head")),getPoiById:function(e){return this.bus.send("poi/getById",{id:e}).then((e=>e[0]))},orphanTest:navGraphDebug.orphanTest,showOrphaned:async function(){const e=await this.bus.get("wayfinder/_getNavGraph"),s=navGraphDebug.orphanTest(e._nodes);this.bus.send("map/showOrphanedGraphNodes",{orphanedNodes:s.orphaned});},showgraph:async function(){const e=await this.bus.get("wayfinder/getNavGraphFeatures");this.bus.send("map/showNavGraphFeatures",{navGraph:e}),r("map/resetNavGraphFeatures",this.bus);},dndGo:function(){const e=this.bus,n=(e,n)=>{e.preventDefault(),n?e.target.classList.add("dragover"):e.target.classList.remove("dragover");},s={drop:async function(s){n(s,!1);for(const n of s.dataTransfer.files){const s=n.name;if("application/json"===n.type||n.type.startsWith("text/")){const t=new FileReader;t.onload=funcs.singleFile((async t=>e.send("debugTools/fileDrop",{file:n,filename:s,content:t.target.result}))),t.readAsText(n);}}},dragover:function(e){n(e,!0);},dragleave:function(e){n(e,!1);}};Object.keys(s).forEach((function(e){document.body.addEventListener(e,s[e]);})),console.log("DnD Listeners installed");},bounds:async function(){const e=await this.bus.get("venueData/getVenueCenter"),{venueRadius:n,bounds:s}=await this.bus.get("venueData/getVenueData");this.bus.send("map/showVenueBounds",{venueCenter:e,venueRadius:n,bounds:s}),r("map/resetVenueBounds",this.bus);},buildingBounds:async function(e){this.bus.send("map/showBuildingBounds",{nameFilter:e}),r("map/resetVenueBounds",this.bus);},padding:async function(){this.bus.send("map/togglePadding"),r("map/togglePadding",this.bus);}};

module.exports = i;
