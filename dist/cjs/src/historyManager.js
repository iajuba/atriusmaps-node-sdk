'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const e=({bus:e})=>{const t=[];e.on("history/register",(({viewId:e,event:s,params:n={}})=>{t.push({viewId:e,event:s,params:n});})),e.on("history/stepBack",(()=>{const s=[],n=t.pop();if(!n)return;s.push(n.viewId);for(let e=t.length-1;e>=0&&t[e].event===n.event;--e){const e=t.pop();s.push(e.viewId);}e.send("layers/hideMultiple",s);const{event:o,params:p}=t.pop();e.send(o,p);})),e.on("history/clean",(()=>{t.length=0;}));};

exports.initHistoryManager = e;
