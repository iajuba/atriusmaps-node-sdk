'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var a = require('zousan');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var a__default = /*#__PURE__*/_interopDefaultLegacy(a);

const n=(o,n)=>o.splice(n,1)[0],t=(o,t)=>{let r=0;do{r=o.indexOf(t,r),r>=0&&n(o,r);}while(r>=0);return o};function r(n){const r=n.log?n.log.sublog?n.log.sublog("bustle",{color:"pink"}):n.log:console,e={},s={};function c(o,n){e[o]&&t(e[o],n);}function u(o,n){s[o]&&t(s[o],n);}function l(t,c){return n.trace&&(c||(c={}),c._stack=Error().stack),n.showEvents&&("function"==typeof n.showEvents?n.showEvents(t,c)&&r.info("send with",t," and ",c):r.info("send with",t," and ",c)),new a__default["default"]((u=>a__default["default"].soon(((t,c,u)=>()=>{const l=e[t],f=s[t],i=[];if(l)for(const t of l)try{i.push(t(c));}catch(t){n.reportAllErrors&&r.error(t),n.rejectOnError&&i.push(a__default["default"].reject(t)),i.push(t);}const h=a__default["default"].all(i);if(f)for(const o of f)try{o(c,h);}catch(o){n.reportAllErrors&&r.error(o);}u(h);})(t,c,u))))}return {get:(n,t)=>l(n,t).then((t=>1!==t.length?a__default["default"].reject(`${n} event did not return a single result, but ${t.length} results.`):t[0])),moff:u,monitor:function(o,n){return s[o]||(s[o]=[]),s[o].push(n),()=>u(o,n)},off:c,on:function(o,n){return e[o]||(e[o]=[]),e[o].push(n),()=>c(o,n)},send:l}}

exports.create = r;
