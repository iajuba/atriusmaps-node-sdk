'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function n(n,o){for(;o.length<n;)o+=" ";return o}const o=()=>n(18,"Command")+"Arguments\n"+n(18,"----------------")+"----------------\n",e=o=>n(18,o.command)+(o.args?o.args.map((n=>`${n.name} {${n.type}} ${n.optional?" (optional)":" (required)"}`)).join(", "):""),r=n=>n.reduce(((n,o)=>`${n}${e(o)}\n`),o());

exports.getHelp = e;
exports.getHelpHeader = o;
exports.getHelpList = r;
