'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e = require('IObject');
var t = require('query-string');
var e$1 = require('ramda');
var i = require('zousan-plus');
var package_json = require('../package.json.js');
var debugTools = require('./debugTools.js');
var env = require('./env.js');
var bustle = require('./extModules/bustle.js');
var log = require('./extModules/log.js');
var i18n = require('./utils/i18n.js');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

function _interopNamespaceDefaultOnly (e) { return Object.freeze({ __proto__: null, 'default': e }); }

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__default = /*#__PURE__*/_interopDefaultLegacy(e);
var t__default = /*#__PURE__*/_interopDefaultLegacy(t);
var i__default = /*#__PURE__*/_interopDefaultLegacy(i);

const g="undefined"!=typeof window;async function p(e,t,n){let o=t;if(o.includes("/")){const e=o.split("/");o=e[e.length-1];}return void 0!==n.active&&!1===n.active?(e.log.info(`Plugin ${t} explicitly deativated`),null):(function (t) { return Promise.resolve().then(function () { return /*#__PURE__*/_interopNamespace(require(t)); }); })(`../plugins/${t}/src/${o}.js`).then((o=>(e.log.info(`Creating plugin ${t}`),o.create(e,n))))}async function d(e){return g?(function (t) { return Promise.resolve().then(function () { return /*#__PURE__*/_interopNamespace(require(t)); }); })(`./configs/${e}.json`):(function (t) { return Promise.resolve().then(function () { return /*#__PURE__*/_interopNamespace(require(t)); }); })(`./configs/${e}.json.js`)}async function m(e,t){let n={};const i=await Promise.all(t.map(d));for(const e of i){let t=e.default;t=t.extends?await m(t,t.extends):t,n=e$1.mergeDeepRight(n,t);}return n=e$1.mergeDeepRight(n,e),n}const f=e=>t=>(function (t) { return Promise.resolve().then(function () { return /*#__PURE__*/_interopNamespace(require(t)); }); })(`./configs/postproc-${e}.js`).then((e=>e.process(t)));async function w(o){const d=Object.create(null);let w=o.extends?await m(o,o.extends):o;w.plugins.monitoring&&Promise.resolve().then(function () { return /*#__PURE__*/_interopNamespaceDefaultOnly(require('../_virtual/_empty_module_placeholder.js')); }).then((e=>e.activate(w))),w=await(async e=>e.configPostProc?i__default["default"].series(e,...e.configPostProc.map(f)):e)(w);const y=(e=>{const n=e.supportedLanguages||["ar","en","es","fr","ja","ko","zh-Hans","zh-Hant"];if("undefined"!=typeof window){let e=t__default["default"].parse(location.search).lang||navigator.language;for(;e;){if(e&&n.includes(e))return e;e=e.substring(0,e.lastIndexOf("-"));}}return e.defaultLanguage||"en"})(w),h=await i18n["default"](y,w.debug);d.i18n=()=>h,d.gt=()=>h.t.bind(h),d.config=w,d.plugins=new e__default["default"];const j=log.initLog("web-engine",{enabled:!!w.debug,isBrowser:g,color:"cyan",logFilter:w.logFilter,truncateObjects:!g});if(d.log=j.sublog(w.name),d.bus=bustle.create({trace:!1,showEvents:!0,reportAllErrors:!0,log:j}),d.info={wePkg:package_json["default"]},"undefined"!=typeof window&&(w.debug?(d.debug=e$1.map((e=>e.bind(d)),debugTools),debugTools.dndGo.call(d)):d.debug={},window._app=d,window.document&&window.document.title&&w.setWindowTitle&&(document.title=w.name)),d.env=env.buildEnv(d),w.theme?await i__default["default"].evaluate({name:"ThemeManagerModule",value:Promise.resolve().then(function () { return /*#__PURE__*/_interopNamespaceDefaultOnly(require('../_virtual/_empty_module_placeholder.js')); })},{name:"HistoryManager",value:Promise.resolve().then(function () { return require('./historyManager.js'); })},{name:"LayerManager",value:Promise.resolve().then(function () { return /*#__PURE__*/_interopNamespaceDefaultOnly(require('../_virtual/_empty_module_placeholder.js')); })}).then((async({LayerManager:e,HistoryManager:t,ThemeManagerModule:n})=>{const o=n.initThemeManager(d);d.themePack=await o.buildTheme(w.theme,w.defaultTheme),e.initLayerManager(d),t.initHistoryManager(d),d.destroy=()=>e.destroy(d);})):d.destroy=()=>{},w.plugins){for(const e in w.plugins)try{const t=w.plugins[e];if(d.plugins[e])throw Error(`Duplicate plugin name "${e}"`);const n=await p(d,e,t);n&&(d.plugins=d.plugins.set(e,n));}catch(t){j.error("Error instantiating plugin "+e),j.error(t);}for(const e in d.plugins)d.plugins[e].init();}return d}

exports.create = w;
