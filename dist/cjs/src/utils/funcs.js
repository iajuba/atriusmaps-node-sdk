'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var a = require('zousan');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var a__default = /*#__PURE__*/_interopDefaultLegacy(a);

let t=null;const e=n=>function(){return t=t?t.then((()=>n.apply(null,arguments))):n.apply(null,arguments),t},o=t=>{const e=e=>new a__default["default"]((n=>setTimeout(n,t,e))),o=new a__default["default"]((n=>setTimeout(n,t)));return e.then=o.then.bind(o),e};function u(n,t){const e={};return Object.keys(t).forEach((o=>{n(o,t[o])&&(e[o]=t[o]);})),e}

exports.delay = o;
exports.filterOb = u;
exports.singleFile = e;
