'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var n = require('i18next');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var n__default = /*#__PURE__*/_interopDefaultLegacy(n);

async function e(e,t){const a=n__default["default"].createInstance();return a.init({debug:t,interpolation:{escapeValue:!1},resources:{en:{translation:{"Walk xx minutes to":"Walk {{count}} minute to","Walk xx minutes to_plural":"Walk {{count}} minutes to"}}},fallbackLng:"en",lng:e,postProcess:["pseudoTrans"]}).then((()=>(t&&a.on("missingKey",(function(n,e,t,o){console.error("i18nNext: key not found. Language: "+a.language+", Key: "+t+", namespace: "+e+", lngs: ",n);})),a)))}const t={ar:"بال عن أمام الأخذ البرية. ثم كما وقام شاسعة الجنوبي, أمدها الإمداد عن انه. ٣٠ فكانت والكساد انه. أساسي الشمال الإتفاقية بال أم. زهاء المؤلّفة حين عل, سكان قررت هذه ان, شعار باستخدام ولم مع.\n\nدول الأمور الصفحة الأوروبية قد. تم حدى فاتّبع الهجوم, ما أوسع معاملة فعل, خلاف الأوروبية، إيو كل. تُصب واندونيسيا، حين قد. أي حاول حقول وهولندا، هذا, بين عن لهيمنة بالسيطرة.\n\nأعمال الإتحاد العمليات من وصل. وقوعها، باستخدام قد أخر, بل حتى علاقة التبرعات. فقد لم الأرض استبدال. حتى الخاسر اقتصادية بـ. أصقاع أعلنت اتفاقية تلك بل, كان إذ بحشد أخرى ليبين. الشتاء، اليابان، مكن أم, به، قد العالم، أوراقهم, بل لعدم لغزو أواخر مكن.\n",ja:"私は場合いよいよある所有らってのの末がしたた。無論結果に圧迫顔もよくその発展ないですまでを決するてみるでしでも発展云いませたから、そうには描いたないたた。機会へ載っない方は別に今をひょろひょろでずまし。むしろ嘉納さんを意味方面そう＃「を積んない権力この自分何か記念をというおお尋ねだろででですで、ある今はこれか頼み例があるで、ネルソンさんののの引込のあなたにもっともお努力といと私赤に実建設が起っようにとやかくご附随を描いですましから、どうもとにかく相違に云おでてならますのが行っないませ。"},a=(n,e)=>"en"===e?n:o(n,t[e]),o=(n,e)=>n.length<e.length?e.substring(0,n.length):e;

exports["default"] = e;
exports.toLang = a;
