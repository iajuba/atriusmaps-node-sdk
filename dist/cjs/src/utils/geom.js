'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var e$1 = require('ramda');

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e$1);

const e=(n,e)=>{const t=n[0],r=n[1];let l=!1;for(let n=0,o=e.length-1;n<e.length;o=n++){const u=e[n][0],s=e[n][1],c=e[o][0],i=e[o][1];s>r!=i>r&&t<(c-u)*(r-s)/(i-s)+u&&(l=!l);}return l};function t(t,l,u,s){if(!e__namespace.length(t))return null;t=t.filter((n=>null==n.shouldDisplay||!0===n.shouldDisplay));const c=e__namespace.curry(e)([l,u]),i=t.filter(e__namespace.compose(c,r));if(0===i.length)return null;if(1===i.length&&!s)return e__namespace.head(i);const d=i.filter(e__namespace.compose(c,e__namespace.prop("boundsPolygon")));return 1===d.length?e__namespace.head(d):d.length?o(i):s?null:o(i)}const r=n=>{const{n:e,s:t,e:r,w:l}=n.bounds;return [[e,r],[e,l],[t,l],[t,r]]},l=n=>{if(!n.bounds)return 0;const{n:e,s:t,e:r,w:l}=n.bounds;return Math.abs((e-t)*(r-l))},o=n=>{return n.reduce((e=l,(n,t)=>e(n)<e(t)?n:t));var e;},u=(n,e)=>Object.values(n.levels).reduce(((n,t)=>t.ordinal===e?t:n),void 0),s=(n,e)=>n.reduce(((n,t)=>Object.values(t.levels).reduce(((n,t)=>t.id===e?t:n),null)||n),void 0);function c(n,e,r,l,o){const s=t(n,e,r,o);return {building:s,floor:s?u(s,l):null}}const i=(n,e,t,r,l)=>c(n,e,t,r,l).floor;function d(n,e,t,r,l,o,u,s){let c=0,i=0,d=0,f=0,a=0;const h=[{x:n,y:e}];for(let g=1,p=0;g<=20;++g)p=g/20,c=1-p,i=c*c,d=i*c,f=p*p,a=f*p,h.push({x:d*n+3*i*p*t+3*c*f*l+a*u,y:d*e+3*i*p*r+3*c*f*o+a*s});return h}

exports.bezierCurveTo = d;
exports.getBuildingAndFloorAtPoint = c;
exports.getFloor = s;
exports.getFloorAt = i;
exports.getStructureAtPoint = t;
exports.ordToFloor = u;
exports.pointInPolygon = e;
