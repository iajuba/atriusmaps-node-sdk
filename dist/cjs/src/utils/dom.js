'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

const t=(t,e)=>"string"==typeof t?(e||document).querySelector(t):t||null,e=(t,e)=>Array.prototype.slice.call((e||document).querySelectorAll(t));function n(t,e){const n=t||{};e=e||document.body;let o="div";n.tag&&(o=n.tag);const s=document.createElement(o);for(const t in n)null!=n[t]&&("klass"===t?s.setAttribute("class",n.klass):"tag"===t||("styles"===t?l(s,n.styles):"text"===t?s.textContent=n.text:"html"===t?s.innerHTML=n.html:s.setAttribute(t,n[t])));return e.appendChild(s),s}const l=(t,e)=>{for(const n in e)t.style[n]=e[n];};

exports.$ = t;
exports.$$ = e;
exports.ad = n;
exports.setStyles = l;
