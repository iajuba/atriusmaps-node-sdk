'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

require('query-string');

const e={navFrom:"online/getDirectionsFromTo",navTo:"online/getDirectionsFromTo",accessible:"online/getDirectionsFromTo",showNav:"online/getDirectionsFromTo",poiId:["online/getDirectionsFromTo","online/poiView"],vid:"venueDataLoader",stage:"venueDataLoader",contentStage:"venueDataLoader",search:"online/headerOnline",ho:["online/getDirectionsFromTo","analytics2"],home:"online/homeView",zoom:"mapRenderer",pitch:"mapRenderer",bearing:"mapRenderer",lat:"mapRenderer",lng:"mapRenderer",radius:"mapRenderer",buildingId:"mapRenderer",floorId:"mapRenderer",refInstallId:"analytics2"};function n(n,o,r){if(void 0!==o.lldebug)try{n.debug=JSON.parse(o.lldebug),null===n.debug&&(n.debug={});}catch(e){n.debug=!0;}return Object.keys(e).forEach((i=>{if(void 0!==o[i]){let a=e[i];Array.isArray(a)||(a=[a]),a.forEach((e=>{let a=n.plugins[e];!a&&r&&(a=n.plugins[e]={}),a.deepLinkProps={...a.deepLinkProps,[i]:o[i]};}));}})),o.poiId&&o.showNav&&delete n.plugins["online/poiView"].deepLinkProps.poiId,n}

exports.setDeepLinksForParms = n;
