'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

require('query-string');
var e = require('ramda');
require('zousan');

function _interopNamespace(e) {
	if (e && e.__esModule) return e;
	var n = Object.create(null);
	if (e) {
		Object.keys(e).forEach(function (k) {
			if (k !== 'default') {
				var d = Object.getOwnPropertyDescriptor(e, k);
				Object.defineProperty(n, k, d.get ? d : {
					enumerable: true,
					get: function () { return e[k]; }
				});
			}
		});
	}
	n["default"] = e;
	return Object.freeze(n);
}

var e__namespace = /*#__PURE__*/_interopNamespace(e);

function r(r,i,o){const p=JSON.parse(i);return Object.values(p).forEach((i=>{let p=r.plugins[i.id];if(!p&&o&&(p=r.plugins[i.id]={}),p){const r=p.deepLinkProps;p.deepLinkProps=r?e__namespace.mergeDeepRight(r,i):i;}})),r}

exports.setStateFromStateString = r;
