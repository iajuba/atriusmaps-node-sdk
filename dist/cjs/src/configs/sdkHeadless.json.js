'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var name = "SDK headless";
var plugins = {
	analytics2: {
		productName: "LocusMaps JS SDK",
		active: false,
		disableSending: false
	},
	clientAPI: {
	},
	dynamicPois: {
	},
	poiDataManager: {
	},
	sdkServer: {
	},
	searchService: {
	},
	venueDataLoader: {
		assetStage: "prod",
		formatVersion: "v5",
		availableLanguages: [
			{
				langCode: "en",
				assetSuffix: ""
			}
		]
	},
	wayfinder: {
		compareFindPaths: false
	}
};
var sdkHeadless = {
	name: name,
	plugins: plugins
};

exports["default"] = sdkHeadless;
exports.name = name;
exports.plugins = plugins;
