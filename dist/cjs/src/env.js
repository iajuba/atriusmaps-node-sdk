'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function e(e){const i=e.config.desktopViewMinWidth||0,n="undefined"!=typeof window,s={isBrowser:n,isMobile:()=>n&&innerWidth<i,isDesktop:()=>n&&!s.isMobile()};return n&&window.addEventListener("resize",(()=>e.bus.send("env/resize"))),s}

exports.buildEnv = e;
